# Pré-requisitos Kubespray

Acesso a internet em todos os NODES para download do repositorio.

1 - Criar relação de confiança entre o servidor do kubespray e os servidores que terão o kubernets instalado.

# Relação de confiança root - root recomendada.

Pacotes necessarios:

    ansible / python3.X / pip / git / jinja / tar.


2 - Baixar o repositorio do kubespray do git:
    git clone https://github.com/kubernetes-incubator/kubespray.git

2.1 - instalar os requeiriments do kubespray:
    cd kubespray
    pip install -r requirements.txt

3 - Fazer a copia do diretorio de inventario exemplo para o inventario SEU_CLUSTER
    cp -rp inventory/sample inventory/MEU_CLUSTER

4 - Preparar os arquivos de yml de configuação:
    Ajustar o etcd no arquivo: inventory/MEU_CLUSTER/group_vars/all/all.yml
    #O SED abaixo faz a alteração 
    sed -i 's/etcd_kubeadm_enabled: false/etcd_kubeadm_enabled: true/' inventory/MEU_CLUSTER/group_vars/all/all.yml

4.1 Alterar o networking:
    Caminho: inventory/localhosts/group_vars/k8s-cluster/k8s-cluster.yml
    #O SED abaixo altera o networking de calico para flannel
    sed -i 's/kube_network_plugin: calico/kube_network_plugin: flannel/' inventory/localhosts/group_vars/k8s-cluster/k8s-cluster.yml
    # Alterar o gerenciador de container de DOCKER pra CONTAINERD
    sed -i 's/ container_manager: docker/ container_manager: containerd/' inventory/localhosts/group_vars/k8s-cluster/k8s-cluster.yml

    
5 - Preparar o inventario para a instalação do ambiente:
    export KUBE_MASTERS_MASTERS=X #X é o numero de MASTERS que você vai criar.
    # Declare quantos IPS serao usados para criar o cluster, separar eles usando apenas ESPAÇO
    declare -a IPS=(XXX.XXX.XXX.XXX XXX.XXX.XXX.XXX XXX.XXX.XXX.XXX QUANTOS_IPS_FOREM)

6 - Executar a preparação do inventario:
    # Atentar para o PYTHON que pode mudar de acordo com a versao disponivel no servidor.
    CONFIG_FILE=inventory/MEU_CLUSTER/hosts.yml python36 contrib/inventory_builder/inventory.py ${IPS[@]}
    
7 - Ajustando o inventory.ini e hosts.yml
    #dentro do inventory.ini fazer as alterações:
        no [all] remover os comentarios de todos os nodes que forem ser utilizados.
        Alterar o ansible_host=XXX.XXX.XXX.XXX para os IPs do seu cluter.
    # Exemplo abaixo
        # ## Configure 'ip' variable to bind kubernetes services on a
        # ## different ip than the default iface
        # ## We should set etcd_member_name for etcd cluster. The node that is not a etcd member do not need to set the value, or can set the empty string value.
        [all]
        node1 ansible_host=192.168.0.183  # ip=10.3.0.1 etcd_member_name=etcd1
        node2 ansible_host=192.168.0.184  # ip=10.3.0.2 etcd_member_name=etcd2
        node3 ansible_host=192.168.0.185  # ip=10.3.0.3 etcd_member_name=etcd3
        # node4 ansible_host=95.54.0.15  # ip=10.3.0.4 etcd_member_name=etcd4
        # node5 ansible_host=95.54.0.16  # ip=10.3.0.5 etcd_member_name=etcd5
        # node6 ansible_host=95.54.0.17  # ip=10.3.0.6 etcd_member_name=etcd6

    # Manter o resto como esta
7.1 - Ajustando o hosts.yml
    # Dentro do arquivo, adicionar em kube_control_plane: todos os nodes que voce ira utilizar como master.
    # Exemplo abaixo
        all:
          hosts:
            node1:
              ansible_host: 192.168.0.183
              ip: 192.168.0.183
              access_ip: 192.168.0.183
            node2:
              ansible_host: 192.168.0.184
              ip: 192.168.0.184
              access_ip: 192.168.0.184
            node3:
              ansible_host: 192.168.0.185
              ip: 192.168.0.185
              access_ip: 192.168.0.185
          children:
            kube_control_plane:
              hosts:
                node1:
                node2:
                node3:
            kube_node:
                hosts:
                node1:
                node2:
                node3:
            etcd:
              hosts:
                node1:
                node2:
                node3:
            k8s_cluster:
              children:
                kube_control_plane:
                kube_node:
            calico_rr:
               hosts: {}

8 - Depois das alterações, executar o playbook conforme a linha abaixo:
    ansible-playbook -i inventory/clusterOVD/hosts.yml --become --become-user=root --flush-cache cluster.yml

O processo pode demorar para executar ate o final.
Ao terminar, executar o comando de validação do cluster de kubernets
    kubectl get nodes
    
@@@@@@@@@ OBS: @@@@@@@@@ 

Caso o fim da instalação esteja com erro no check do etcd, fazer o reboot de todos os servidores do cluster e executar o processo novamente.
